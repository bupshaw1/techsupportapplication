﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport.View
{
    public partial class View_TechSupport : Form
    {
        public View_TechSupport()
        {
            InitializeComponent();
        }
        View_IncidentsReport child5;

        private void child_FormClosed(object sender, FormClosedEventArgs e)
        {
            child5 = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void displayIncidentsByProductsAndTechnicianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (child5 == null)
            {
                child5 = new View_IncidentsReport();
                child5.MdiParent = this;
                child5.FormClosed += new FormClosedEventHandler(child_FormClosed);
                child5.Show();
            }
            else
            {
                child5.Activate();
            }
        }
    }
}
